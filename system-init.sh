#!/usr/bin/env bash

### add ls aliases to bashrc
echo "alias ll='ls -l'" >> ~/.bashrc
echo "alias la='ls -A'" >> ~/.bashrc
echo "alias l='ls -CF'" >> ~/.bashrc

### ensure up-to-date system
apt-get update && apt-get upgrade
apt-get install curl mc unattended-upgrades apt-listchanges aptitude sudo screen git virtual-mysql-client python3-pip

### make basic git configuration
read -p "Please enter user name for git commits:" git_user_name
read -p "Please enter email for git commits:" git_user_email
git config --global user.email "$git_user_email"
git config --global user.name "$git_user_name"

### setup ssh key
# This script will inject the private ssh key entered by the user and compute the resulting pub key
echo "Please enter your private ssh key followed by [CTRL-d]:"
sshkey=$(</dev/stdin)
echo "$sshkey" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub

### install docker
curl -sSL https://get.docker.com/ | CHANNEL=stable sh
systemctl enable docker.service
systemctl start docker.service

### docker-compose
pip3 install docker-compose

### initialize system
cd /opt
read -p "Enter git username:" git_account
read -p "Enter system config git repository:" git_system_repo

git clone git@gitlab.com:$git_account/$git_system_repo.git
cd $git_system_repo
bash generate_config.sh
docker-compose pull
docker-compose up -d

